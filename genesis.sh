#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
NODE_DIR="$1"

function die {
    >&2 echo "$@"
    exit 1
}

[ -n "$NODE_DIR" ] || die "Usage: $0 <node dir>"
[ -d "$NODE_DIR" ] || die "$NODE_DIR is not a directory"
PART=$(sed -nE 's/^\s*partition-id\s*=\s*"(.*)"$/\L\1/p' "$NODE_DIR/bvnn/config/accumulate.toml") && [ -n "$PART" ] || die "Unable to determine partition"

rm -rf $NODE_DIR/dnn/data/{*.db,cs.wal}
cat "$SCRIPT_DIR/priv_validator_state.json" > "$NODE_DIR/dnn/data/priv_validator_state.json"
cat "$SCRIPT_DIR/genesis-directory.json" > "$NODE_DIR/dnn/config/genesis.json"

rm -rf $NODE_DIR/bvnn/data/{*.db,cs.wal}
cat "$SCRIPT_DIR/priv_validator_state.json" > "$NODE_DIR/bvnn/data/priv_validator_state.json"
cat "$SCRIPT_DIR/genesis-$PART.json" > "$NODE_DIR/bvnn/config/genesis.json"
