#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
NODE_DIR="$1"

function die {
    >&2 echo "$@"
    exit 1
}

[ -n "$NODE_DIR" ] || die "Usage: $0 <node dir>"
[ -d "$NODE_DIR" ] || die "$NODE_DIR is not a directory"
PART=$(sed -nE 's/^\s*partition-id\s*=\s*"(.*)"$/\L\1/p' "$NODE_DIR/bvnn/config/accumulate.toml") && [ -n "$PART" ] || die "Unable to determine partition"

BVN0=c191f63b27925c76e5d72c123356dca4c7ce59f1@127.0.2.1
BVN1=ccda0f056391b031f8bb629ea46c178d55c1457d@127.0.2.2
BVN2=137e8287f7be2489d7f26987d83397b33f3b0fb9@127.0.2.3

sed -i 's/^bootstrap-peers = ".*"$/bootstrap-peers = "'$BVN0':16591,'$BVN1':16591,'$BVN2':16591"/' "$NODE_DIR/dnn/config/tendermint.toml"

case $PART in
    bvn0) sed -i 's/^bootstrap-peers = ".*"$/bootstrap-peers = "'$BVN0':16591"/' "$NODE_DIR/bvnn/config/tendermint.toml" ;;
    bvn1) sed -i 's/^bootstrap-peers = ".*"$/bootstrap-peers = "'$BVN1':16591"/' "$NODE_DIR/bvnn/config/tendermint.toml" ;;
    bvn2) sed -i 's/^bootstrap-peers = ".*"$/bootstrap-peers = "'$BVN2':16591"/' "$NODE_DIR/bvnn/config/tendermint.toml" ;;
esac